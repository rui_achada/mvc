﻿using mvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace mvc.Controllers
{
    public class FilmesController : Controller
    {
        DataClasses1DataContext dc = new DataClasses1DataContext();

        // GET: Filmes
        public ActionResult Index()
        {
            return View(dc.Filmes);
        }

        // GET: Filmes/Details/5
        public ActionResult Details(int? id)
        {
            if(id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Filme filme = dc.Filmes.FirstOrDefault(f => f.id == id);

            if(filme == null)
            {
                return HttpNotFound();
            }

            Categoria categoria = dc.Categorias.FirstOrDefault(c => c.Sigla == filme.categoria);

            if(categoria != null)
            {
                ViewBag.CategoriaPorExtenso = categoria.Categoria1; // para podermos passar o valor da categoria para a view (porque a view não permite mais que um modelo)
            }

            return View(filme);
        }

        // GET: Filmes/Create
        public ActionResult Create()
        {
            // lista que vai à tabela categorias. O valor guardado vai ser Sigla mas o que aparece na comboBox é a Categoria1
            ViewBag.SiglaCategoria = new SelectList(dc.Categorias.OrderBy(c => c.Categoria1), "Sigla", "Categoria1");

            return View();
        }

        // POST: Filmes/Create
        [HttpPost]
        public ActionResult Create(Filme novoFilme)
        {
            novoFilme.id = dc.Filmes.Count() + 1;

            if(ModelState.IsValid)
            {
                dc.Filmes.InsertOnSubmit(novoFilme);
            }

            try
            {
                dc.SubmitChanges();

                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                ViewBag.Erro = e;
                return View();
            }
        }

        // GET: Filmes/Edit/5
        public ActionResult Edit(int? id)
        {
            ViewBag.SiglaCategoria = new SelectList(dc.Categorias.OrderBy(c => c.Categoria1), "Sigla", "Categoria1");

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Filme filme = dc.Filmes.FirstOrDefault(f => f.id == id);

            if(filme == null)
            {
                return HttpNotFound();
            }

            return View(filme);
        }

        // POST: Filmes/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Filme filme)
        {
            Filme filmeAlterado = dc.Filmes.FirstOrDefault(f => f.id == id);

            if(filmeAlterado == null)
            {
                return HttpNotFound();
            }

            Categoria categoria = dc.Categorias.FirstOrDefault(c => c.Sigla == filme.categoria);

            if(ModelState.IsValid && categoria != null)
            {
                filmeAlterado.titulo = filme.titulo;
                filmeAlterado.categoria = categoria.Sigla;
            }

            try
            {
                dc.SubmitChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Filmes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Filme filme = dc.Filmes.FirstOrDefault(f => f.id == id);

            if (filme == null)
            {
                return HttpNotFound();
            }

            return View(filme);
        }

        // POST: Filmes/Delete/5
        [HttpPost]
        public ActionResult Delete(int id)
        {
            Filme filme = dc.Filmes.FirstOrDefault(f => f.id == id);

            if (filme == null)
            {
                return HttpNotFound();
            }

            dc.Filmes.DeleteOnSubmit(filme);

            try
            {
                dc.SubmitChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
