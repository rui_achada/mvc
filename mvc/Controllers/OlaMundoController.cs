﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace mvc.Controllers
{
    public class OlaMundoController : Controller
    {
        //// GET: OlaMundo
        //public string Index()
        //{
        //    return "Esta é a minha Action por <b>defeito</b>";
        //}

        //// GET: OlaMundo/Welcome
        //public string Welcome()
        //{
        //    return "Esta é o método que a Action Welcome chama";
        //}

        //public string Welcome(string nome, int numVezes = 1)
        //{
        //    return HttpUtility.HtmlEncode("Olá " + nome + ", Número de vezes: " + numVezes);
        //}

        //// GET: OlaMundo/Welcome/ID?nome=Rafael
        //public string Welcome(string nome, int ID = 1)
        //{
        //    return HttpUtility.HtmlEncode("Olá " + nome + ", Número de vezes: " + ID);
        //}

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Welcome(string nome, int numVezes = 1)
        {
            ViewBag.Mensagem = "Olá " + nome;
            ViewBag.NumeroVezes = numVezes;

            return View();
        }
    }
}