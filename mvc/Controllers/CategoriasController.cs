﻿using mvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace mvc.Controllers
{
    public class CategoriasController : Controller
    {
        DataClasses1DataContext dc = new DataClasses1DataContext(); // ligação à base de dados

        // GET: Categorias
        public ActionResult Index()
        {
            return View(dc.Categorias);
        }

        //// GET: Categorias/Details/5
        //public ActionResult Details(int id)
        //{
        //    return View();
        //}

        // GET: Categorias/Create
        public ActionResult Create() // Vista que é lançada quando entramos no create
        {
            return View();
        }

        // POST: Categorias/Create
        [HttpPost]
        public ActionResult Create(Categoria novaCategoria)
        {
            if(ModelState.IsValid)
            {
                dc.Categorias.InsertOnSubmit(novaCategoria);
            }

            try
            {
                dc.SubmitChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                return View(); // Se correr mal retorna para a vista normal
            }
        }

        // GET: Categorias/Edit/5
        public ActionResult Edit(string sigla)
        {
            if (sigla == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Categoria categoria = dc.Categorias.FirstOrDefault(c => c.Sigla == sigla);

            if (categoria == null) // Se o ID não existir
            {
                return HttpNotFound();
            }

            return View(categoria);
        }

        // POST: Categorias/Edit/5
        [HttpPost]
        public ActionResult Edit(string sigla, Categoria categoriaAlterar)
        {
            if (categoriaAlterar == null)
            {
                return HttpNotFound();
            }

            Categoria categoriaAlterada = dc.Categorias.FirstOrDefault(c => c.Sigla == sigla);

            if (categoriaAlterada == null)
            {
                return HttpNotFound();
            }

            if (ModelState.IsValid)
            {
                categoriaAlterada.Categoria1 = categoriaAlterar.Categoria1;
            }

            try
            {
                dc.SubmitChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Categorias/Delete/FA
        public ActionResult Delete(string sigla)
        // Para o código ser executado mesmo quando é null, usamos int?
        // https://stackoverflow.com/questions/20989484/what-does-question-mark-means-in-mvc
        {
            //Categoria categoria = dc.Categorias.FirstOrDefault(c => c.Sigla == sigla);

            //if(categoria != null)
            //{
            //    dc.Categorias.DeleteOnSubmit(categoria);

            //    try
            //    {
            //        dc.SubmitChanges();
            //    }
            //    catch (Exception e)
            //    {
            //        return View();
            //    }
            //}

            if (sigla == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Categoria categoria = dc.Categorias.FirstOrDefault(c => c.Sigla == sigla);

            if(categoria == null) // Se o ID não existir
            {
                return HttpNotFound();
            }

            return View(categoria);
        }

        // Foi necessário mudar o nome do método para DeletePost para não fazer conflito com o anterior
        // https://stackoverflow.com/questions/20043400/already-defines-a-member-called-create-with-the-same-parameter-types

        // POST: Categorias/Delete/5
        [HttpPost]
        [ActionName("Delete")]
        public ActionResult DeletePost(string sigla)
        {
            Categoria categoria = dc.Categorias.FirstOrDefault(c => c.Sigla == sigla);

            if(categoria == null)
            {
                return HttpNotFound();
            }

            dc.Categorias.DeleteOnSubmit(categoria);

            try
            {
                dc.SubmitChanges();

                return RedirectToAction("Index"); // Apaga e redireciona para a página Index
            }
            catch
            {
                return View(); // Caso haja um problema, faz o catch e mantém-se na mesma view
            }
        }
    }
}
